//=============================================================================
// MeasurementManager.cpp
//=============================================================================
// abstraction.......MeasurementManager for Keithley3706
// class.............MeasurementManager
// original author...E.Lombard - Nexeya
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat4tango/LogHelper.h>
#include <math.h>
#include "MeasurementManager.h"

namespace Keithley3706_ns
{
//*****************************************************************************
// MeasureScanThread
//*****************************************************************************
// ============================================================================
// MeasureScanThread::MeasureScanThread
// ============================================================================
MeasureScanThread::MeasureScanThread (const MeasureScanConfig & cfg, yat::Thread::IOArg ioa)
	: yat::Thread(ioa),
	Tango::LogAdapter (cfg.hostDevice), 
	m_cfg (cfg),
	m_goOn(true)
{
	//- noop ctor
}

// ============================================================================
// MeasureScanThread::~MeasureScanThread
// ============================================================================
MeasureScanThread::~MeasureScanThread (void)
{
	//- noop dtor
}

// ============================================================================
// MeasureScanThread::run_undetached
// ============================================================================
yat::Thread::IOArg MeasureScanThread::run_undetached (yat::Thread::IOArg ioa)
{
	DEBUG_STREAM << "MeasureScanThread::run_undetached() entering... " << std::endl;

	//- get ref. to out our parent task
	MeasurementManager * poMeasurementManagerTask = reinterpret_cast<MeasurementManager *>(ioa);

	//- Loop to wait for the measurement of all the channels by the hardware
	poMeasurementManagerTask->DMM_MeasurementWaitingLoop();

	// when function returns, the measurement are completed
	DEBUG_STREAM << "HW measurements completed!" << std::endl;

	// send a "END of HW Measurements" message to the MeasurementManager task		
	// define message
	yat::Message * msg = new yat::Message(kEND_OF_HW_MEASUREMENTS);
	if (! msg)
	{
		ERROR_STREAM << "MeasureScanThread::run_undetached yat::Message allocation failed!" << std::endl; 
		return 0;
	}

	//- post it to the task
	poMeasurementManagerTask->post(msg);

	return 0;
}


// ============================================================================
// MeasureScanThread::exit
// ============================================================================
void MeasureScanThread::exit (void)
{
	this->m_goOn = false;
}

//- check proxy macro:
#define CHECK_PROXY() \
do \
{ \
if (! m_hw_proxy) \
{ \
yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus); \
m_isInternalPb = true; \
m_strStatus = "request aborted - the Proxy isn't accessible"; \
THROW_DEVFAILED("DEVICE_ERROR", \
"request aborted - the Proxy isn't accessible ", \
"MeasurementManager::CHECK_PROXY"); \
} \
} while (0)


// ============================================================================
// MeasurementManager::MeasurementManager ()
// ============================================================================ 
MeasurementManager::MeasurementManager (Tango::DeviceImpl * hostDevice)
	: yat4tango::DeviceTask(hostDevice)
{
	//- trace/profile this method
	yat4tango::TraceHelper t("MeasurementManager::MeasurementManager", this);

	m_oHostDevice = hostDevice;
	m_hw_proxy = NULL;
	m_oState = Tango::INIT;
	m_strStatus = "Initialization in progress...";
	m_poMeasurementScanThread = NULL;
	m_isInAlarm = false;
	m_isInternalPb = false;
	m_bScanAborted = false;

	//- No periodic job allowed yet
	enable_periodic_msg(false);
}

// ============================================================================
// MeasurementManager::~MeasurementManager ()
// ============================================================================ 
MeasurementManager::~MeasurementManager(void)
{
	//- No more periodic job allowed
	enable_periodic_msg(false);

	if (m_hw_proxy)
	{
		delete m_hw_proxy;
		m_hw_proxy = NULL;
	}
}

// ============================================================================
// MeasurementManager::get_ScanAborted ()
// ============================================================================ 
bool MeasurementManager::get_ScanAborted()
{
	yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
	return m_bScanAborted;
}

// ============================================================================
// MeasurementManager::get_state ()
// ============================================================================ 
Tango::DevState MeasurementManager::get_state()
{
	update_state();

	yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
	return m_oState;
}

// ============================================================================
// MeasurementManager::get_status ()
// ============================================================================ 
std::string MeasurementManager::get_status()
{
	update_state();

	yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
	return m_strStatus;
}

// ============================================================================
// MeasurementManager::update_state ()
// ============================================================================ 
void MeasurementManager::update_state()
{
	Tango::DevState oState;

	try
	{
		// get hw proxy state
		oState = get_device_state();
	}
	catch(...)
	{
		oState = Tango::FAULT;
	}

	// compute manager state according to proxy state and internal state
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		switch(oState)
		{
		case Tango::OPEN:
			if (m_isInternalPb)
			{
				m_oState = Tango::FAULT;
			}
			else if (m_isInAlarm)
			{
				m_oState = Tango::ALARM;
			}
			else
			{
				m_oState = Tango::RUNNING;
			}
			break;
		default:
			m_oState = Tango::FAULT;
			break;
		}
	}
}

// ============================================================================
// MeasurementManager::get_device_state ()
// ============================================================================ 
Tango::DevState MeasurementManager::get_device_state()
{
	Tango::DevState oState;

	CHECK_PROXY();

	// get hw proxy state
	try
	{
		oState = m_hw_proxy->state();
	}
	catch (Tango::DevFailed &e)
	{
		oState = Tango::FAULT;
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_strStatus = "Access to proxy device failed - failed to get state of device proxy";
		ERROR_STREAM << e << std::endl;
	}
	catch (...)
	{
		oState = Tango::FAULT;
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_strStatus = "Access to proxy device failed - failed to get state of device proxy (undetermined error)";
		ERROR_STREAM << m_strStatus << std::endl;
	}

	return (oState);
}


//--------------------------------------------------------
/**
*	Command init_hw related method
*	Description: Init the measurement manager
*
*	@param strProxyName (std::string) Name of the proxy device
*	@param strSettingFileName (std::string) Complete name of the DMM Setup file
*	@param oConfigMap (Keithley3706ConfigMap_t) Original map of channels parameters
*	@param dPollingPeriod (double) Polling period (ms)
*/
//--------------------------------------------------------
void MeasurementManager::init_hw(std::string strProxyName
	, std::string strSettingFileName
	, Keithley3706ConfigMap_t oConfigMap
	, double dPollingPeriod
	, double historyDurationInHour)
{
	DEBUG_STREAM << "MeasurementManager::init_hw / Entering!!!" << std::endl;
	DEBUG_STREAM << "-> strProxyName       = \"" << strProxyName << "\"" << std::endl;
	DEBUG_STREAM << "-> strSettingFileName = \"" << strSettingFileName << "\"" << std::endl;
	DEBUG_STREAM << "-> dPollingPeriod     = " << dPollingPeriod << std::endl;
	DEBUG_STREAM << "-> historyDurationInHour     = " << historyDurationInHour << std::endl;
	m_history_size = (historyDurationInHour * 60) / ( dPollingPeriod / 60000);
	DEBUG_STREAM << "-> m_history_size     = " << m_history_size << std::endl;

	//- Create device proxy
	try
	{
		m_hw_proxy = new Tango::DeviceProxy(strProxyName);
	}
	catch (Tango::DevFailed &e)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInternalPb = true;
		m_strStatus = "Initialization failed - failed to create device proxy";
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			m_strStatus.c_str(), 
			"MeasurementManager::init_hw"); 
	}
	catch (...)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInternalPb = true;
		m_strStatus = "Initialization failed - failed to create device proxy (undetermined error)";
		ERROR_STREAM << m_strStatus << std::endl;
		THROW_DEVFAILED("DEVICE_ERROR", 
			m_strStatus.c_str(), 
			"MeasurementManager::init_hw"); 
	}

	//- Update the state and status of the proxy device, after verifying its accessibility
	CHECK_PROXY();
	try
	{
		update_state();
	}
	catch (Tango::DevFailed &e)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInternalPb = true;
		m_strStatus = "Initialization failed - failed to update device state and status";
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			m_strStatus.c_str(),
			"MeasurementManager::init_hw"); 
	}
	catch (...)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		std::stringstream ssErrorMsg;
		ssErrorMsg  << "Initialization failed - failed to update device state and status (undetermined error)" << std::endl;
		m_isInternalPb = true;
		m_strStatus = ssErrorMsg.str();
		ERROR_STREAM << ssErrorMsg.str();
		THROW_DEVFAILED("DEVICE_ERROR", 
			ssErrorMsg.str().c_str(), 
			"MeasurementManager::init_hw"); 
	}

	//- Init the parameters for the calculation of the temperature
	init_temp_params(oConfigMap);

	//- Init the hardware with commands of the settings file
	try
	{
		DMM_Setup(strSettingFileName);
	}
	catch (Tango::DevFailed &e)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInternalPb = true;
		m_strStatus = "DMM setup failed - failed to initialize the DMM";
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			m_strStatus.c_str(),
			"MeasurementManager::init_hw"); 
	}
	catch (...)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		std::stringstream ssErrorMsg;
		ssErrorMsg  << "DMM setup failed - failed to initialize the DMM (undetermined error)" << std::endl;
		m_isInternalPb = true;
		m_strStatus = ssErrorMsg.str();
		ERROR_STREAM << ssErrorMsg.str();
		THROW_DEVFAILED("DEVICE_ERROR", 
			ssErrorMsg.str().c_str(), 
			"MeasurementManager::init_hw"); 
	}

	//- Starting the task
	enable_periodic_msg(true);

	//- Setting the polling period of the current task
	set_periodic_msg_period((size_t)dPollingPeriod); // ms

	//- Specifying the current task has not been aborted
	m_bScanAborted = false;

	//- The initialization is finished
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_strStatus = "The Keithley is monitoring temperatures";
		m_oState = Tango::RUNNING;
	}
}

//--------------------------------------------------------
/**
*	Description: Read the setup commands in a file and send it to the hardware
*
*	@param strSettingFileName Complete name of the DMM Setup file
*/
//--------------------------------------------------------
void MeasurementManager::DMM_Setup(std::string strSettingFileName)
{
	DEBUG_STREAM << "MeasurementManager::DMM_Setup / Entering!!!" << std::endl;

	//- String lines of the setting file
	std::vector<std::string> vstrInitSettingsCmd;

	//- Init the hardware by sending the commands present on each line of the setting file
	//- Try to open the setting file
	std::ifstream oSettingFile( strSettingFileName.c_str() );
	if(oSettingFile)
	{
		//- The file is open
		//- Try to read each line
		std::string strTmpLine;
		while(getline(oSettingFile, strTmpLine))
		{
			//- Add each read line in the list
			vstrInitSettingsCmd.push_back(strTmpLine);
		}
		oSettingFile.close();

		//- Add two lines containing range configuration
		std::stringstream ssTmpLine;
		ssTmpLine << "dmm.configure.set(\"" << RANGE_CONFIG << "\")";
		strTmpLine = ssTmpLine.str();
		vstrInitSettingsCmd.push_back(strTmpLine);

		ssTmpLine.str("");
		ssTmpLine << "dmm.setconfig(\"" << m_strDMM_Channels << "\",\"" << RANGE_CONFIG << "\")";
		strTmpLine = ssTmpLine.str();
		vstrInitSettingsCmd.push_back(strTmpLine);
	}
	else
	{
		std::stringstream ssErrorMsg;
		ssErrorMsg << "Impossible to open the file \"" << strSettingFileName << "\"" << std::endl;

		ERROR_STREAM << ssErrorMsg.str();
		THROW_DEVFAILED("CONFIGURATION_ERROR",
			ssErrorMsg.str().c_str(),
			"MeasurementManager::DMM_Setup ");
	}

	//- Send each setting line as a command to the hardware
	DEBUG_STREAM << "MeasurementManager::DMM_Setup / Reading file \"" << strSettingFileName << "\" :" << std::endl;
	std::stringstream ssCmdLine;
	for (vector<string>::iterator pstrCurrentLine = vstrInitSettingsCmd.begin(); pstrCurrentLine != vstrInitSettingsCmd.end(); ++pstrCurrentLine)
	{
		ssCmdLine.str("");
		try
		{
			ssCmdLine << *pstrCurrentLine;
			DEBUG_STREAM << "-> strCurrentLine     = \"" << ssCmdLine.str() << "\"" << std::endl;
			DMM_CommandWrite(ssCmdLine);
		}
		catch (Tango::DevFailed &e)
		{
			yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
			m_isInternalPb = true;
			std::stringstream ssErrorMsg;
			ssErrorMsg << "Command for DMM setup not accepted (\"" << *pstrCurrentLine << "\") : see logs for details";
			m_strStatus = ssErrorMsg.str();
			ERROR_STREAM << e << std::endl;
			RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
				ssErrorMsg.str().c_str(),
				"MeasurementManager::DMM_Setup"); 
		}
		catch (...)
		{
			yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
			m_isInternalPb = true;
			std::stringstream ssErrorMsg;
			ssErrorMsg << "Command for DMM setup not accepted (\"" << *pstrCurrentLine << "\") (undetermined error)";
			m_strStatus = ssErrorMsg.str();
			ERROR_STREAM << ssErrorMsg.str();
			THROW_DEVFAILED("DEVICE_ERROR", 
				ssErrorMsg.str().c_str(), 
				"MeasurementManager::DMM_Setup"); 
		}
	}
}

//--------------------------------------------------------
/**
*	Description: Send commands to the hardware to create its reading buffer
*/
//--------------------------------------------------------
void MeasurementManager::DMM_CreateReadingBuffer(void)
{
	DEBUG_STREAM << "MeasurementManager::DMM_CreateReadingBuffer / Entering!!!" << std::endl;

	std::stringstream ssCmdLine;
	try
	{
		//- Send each setting line as a command to the hardware
		ssCmdLine << "MyBuffer=dmm.makebuffer(" << m_mapTempParam.size() << ")";
		DMM_CommandWrite(ssCmdLine);

		DMM_CommandWrite("MyBuffer.appendmode=0");

		//- Empty scan list
		ssCmdLine.str("");
		ssCmdLine << "scan.create(\"" << m_strDMM_Channels << "\")";
		DMM_CommandWrite(ssCmdLine);

		//- 1 measure per scan
		DMM_CommandWrite("scan.measurecount=1");

		//- 1 scan at a time
		DMM_CommandWrite("scan.scancount=1");
	}
	catch (Tango::DevFailed &e)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInternalPb = true;
		m_strStatus = "Reading buffer of the DMM not created: see logs for details";
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"Failed to create the reading buffer of the DMM",
			"MeasurementManager::DMM_CreateReadingBuffer"); 
	}
	catch (...)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInternalPb = true;
		m_strStatus = "Reading buffer of the DMM not created (undetermined error)";
		std::stringstream ssErrorMsg;
		ssErrorMsg  << "Failed to create the reading buffer of the DMM (undetermined error)" << std::endl;
		ERROR_STREAM << ssErrorMsg.str();
		THROW_DEVFAILED("DEVICE_ERROR", 
			ssErrorMsg.str().c_str(), 
			"MeasurementManager::DMM_CreateReadingBuffer"); 
	}
}

//--------------------------------------------------------
/**
*	Description: Send commands to the hardware to scan all the channels once (with storage of the values in its reading buffer)
*                , and finally read the values to store them locally (in Map).
*/
//--------------------------------------------------------
void MeasurementManager::DMM_Measurement(void)
{
	DEBUG_STREAM << "MeasurementManager::DMM_Measurement / Entering!!!" << std::endl;

	try
	{
		//- Initialize and launch the measurement in background mode
		DMM_MeasurementInitialization();

		//- Create thread to let the HW scanning
		MeasureScanConfig oMS_Cfg;
		oMS_Cfg.hostDevice = m_oHostDevice;
		m_poMeasurementScanThread = new MeasureScanThread(oMS_Cfg, static_cast<yat::Thread::IOArg>(this));
		if (this->m_poMeasurementScanThread == NULL)
		{
			ERROR_STREAM << "MeasureScanThread cannot be created!" << std::endl;
			THROW_DEVFAILED("DEVICE_ERROR",
				"Failed to create MeasureScanThread!",
				"MeasurementManager::DMM_Measurement");    
		}
		m_poMeasurementScanThread->start_undetached();
	}
	catch (Tango::DevFailed &e)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInternalPb = true;
		m_strStatus = "Measurement on the DMM not completed: see logs for details";
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"DMM measurement failed - failed to make measurements on the DMM",
			"MeasurementManager::DMM_Measurement"); 
	}
	catch (...)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInternalPb = true;
		m_strStatus = "Measurement on the DMM not completed: see logs for details";
		std::stringstream ssErrorMsg;
		ssErrorMsg  << "DMM measurement failed - failed to make measurements on the DMM (undetermined error)" << std::endl;
		ERROR_STREAM << ssErrorMsg.str();
		THROW_DEVFAILED("DEVICE_ERROR", 
			ssErrorMsg.str().c_str(), 
			"MeasurementManager::DMM_Measurement"); 
	}
}

//--------------------------------------------------------
/**
*	Description: Send commands to the hardware to initialize the measurement in background mode.
*/
//--------------------------------------------------------
void MeasurementManager::DMM_MeasurementInitialization(void)
{
	DEBUG_STREAM << "MeasurementManager::DMM_MeasurementInitialization / Entering!!!" << std::endl;

	try
	{
		//- Clear the DMM buffer
		DMM_CommandWrite("MyBuffer.clear()");

		//- Start measurement procedure
		DMM_CommandWrite("display.clear()");
		DMM_CommandWrite("display.setcursor(1,1,1)");
		DMM_CommandWrite("display.settext(\"Starting Measurements\")");
		DMM_CommandWrite("display.setcursor(2,1,1)");
		DMM_CommandWrite("display.settext(os.date())");

		//- Initiate scanning process (measurement in background mode)
		DMM_CommandWrite("scan.background(MyBuffer)");
	}
	catch (Tango::DevFailed &e)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInternalPb = true;
		m_strStatus = "Initialization of the measurement on the DMM not completed: see logs for details";
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"Failed to make measurement initialization on the DMM",
			"MeasurementManager::DMM_MeasurementInitialization"); 
	}
	catch (...)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInternalPb = true;
		m_strStatus = "Initialization of the measurement on the DMM not completed: see logs for details";
		std::stringstream ssErrorMsg;
		ssErrorMsg  << "Failed to make measurement initialization on the DMM (undetermined error)" << std::endl;
		ERROR_STREAM << ssErrorMsg.str();
		THROW_DEVFAILED("DEVICE_ERROR", 
			ssErrorMsg.str().c_str(), 
			"MeasurementManager::DMM_MeasurementInitialization"); 
	}
}

//--------------------------------------------------------
/**
*	Description: Send commands to the hardware in a loop, to wait for the complete measurements (each channel have to be scanned).
*/
//--------------------------------------------------------
void MeasurementManager::DMM_MeasurementWaitingLoop(void)
{
	DEBUG_STREAM << "MeasurementManager::DMM_MeasurementWaitingLoop / Entering!!!" << std::endl;

	std::string strTmpRead("");
	std::stringstream ssWaitingMsg;
	std::stringstream ssWaitingCmd;
	Tango::DeviceData oDataToRead;
	unsigned int uiNbChannels = m_mapTempParam.size();
	unsigned int uiCurrentCount = 0;
	unsigned int uiNbLoops = 0;
	unsigned int uiTmpLoop = 0;
	
	ssWaitingMsg.str("Scanning");
	
	//- Scanning until all the channels have been set in the hardware buffer
	while( (uiCurrentCount < uiNbChannels) && !get_ScanAborted() )
	{
		try
		{
			//- Delay time
			DEBUG_STREAM << "MeasurementManager::DMM_MeasurementWaitingLoop / (Beginning of loop): CurrentCount = " << uiCurrentCount << " / " << uiNbChannels << std::endl;

			//- Prepare "keep alive" message
			uiTmpLoop = uiNbLoops % 10;
			if (uiTmpLoop == 0)
			{
			  ssWaitingMsg.str("");
			  ssWaitingMsg  <<  "Scanning";
			}
			else
			{
			  ssWaitingMsg  <<  ".";
			}
			ssWaitingCmd.str("");
			ssWaitingCmd << "display.settext(\"" << ssWaitingMsg.str() << "              \")";
			
			// use sleep (time to measure one temperature) instead of reading MyBuffer.n because over 9 channels, 
			// it doesn't seem to work well.                        
			usleep(500000); //us linux
			uiCurrentCount++;
			DEBUG_STREAM << "MeasurementManager::DMM_MeasurementWaitingLoop / (End of loop) CurrentCount = " << uiCurrentCount << " / " << uiNbChannels << std::endl;

		}
		catch (Tango::DevFailed &e)
		{
			yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
			m_isInternalPb = true;
			m_strStatus = "Loop of the measurement on the DMM not completed: see logs for details";
			std::stringstream ssErrorMsg;
			ssErrorMsg  << "Failed to complete waiting loop for measurements on the DMM (CurrentCount = " << uiCurrentCount << " / " << uiNbChannels << ")" << std::endl;
			ERROR_STREAM << e << std::endl;
			RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
				ssErrorMsg.str().c_str(),
				"MeasurementManager::DMM_MeasurementWaitingLoop"); 
		}
		catch (...)
		{
			yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
			m_isInternalPb = true;
			m_strStatus = "Loop of the measurement on the DMM not completed: see logs for details";
			std::stringstream ssErrorMsg;
			ssErrorMsg  << "Failed to complete waiting loop for measurements on the DMM (CurrentCount = " << uiCurrentCount << " / " << uiNbChannels << ") (undetermined error)" << std::endl;
			ERROR_STREAM << ssErrorMsg.str();
			THROW_DEVFAILED("DEVICE_ERROR", 
				ssErrorMsg.str().c_str(), 
				"MeasurementManager::DMM_MeasurementWaitingLoop"); 
		}
	}
}

//--------------------------------------------------------
/**
*	Description: Send commands to the hardware to scan all the channels once (with storage of the values in its reading buffer)
*                , and finally read the values to store them locally (in Map).
*/
//--------------------------------------------------------
void MeasurementManager::DMM_MeasurementGettingValues(void)
{
	DEBUG_STREAM << "MeasurementManager::DMM_MeasurementGettingValues / Entering!!!" << std::endl;

	Tango::DeviceData oDataToRead;
	std::stringstream ssCmdLine;
	double dTmpResistance = 0.0;

	//- Parsing the config map, to read each channel (value read one by one)
	unsigned int uiBufferIdx = 1;
	std::string strTmpRead("");

	yat::AutoMutex<> guard(MeasurementManager::m_lock);
	for (TempParamMap_it_t it = m_mapTempParam.begin(); it != m_mapTempParam.end(); ++it)
	{
		ssCmdLine.str("");
		ssCmdLine << "printbuffer(" << uiBufferIdx << ", " << uiBufferIdx << ", MyBuffer)";

		try
		{
			oDataToRead = DMM_CommandWriteRead(ssCmdLine);
			oDataToRead >> strTmpRead; //- Extract the value
			DEBUG_STREAM << "MeasurementManager::DMM_MeasurementGettingValues / strTmpRead = \"" << strTmpRead << "\"" << std::endl;
		}
		catch (Tango::DevFailed &e)
		{
			yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
			m_isInternalPb = true;
			std::stringstream ssErrorMsg;
			ssErrorMsg  << "Failed to read the value #" << uiBufferIdx << " (\"" << (it->first) << "\") from the DMM: see logs for details";
			m_strStatus = ssErrorMsg.str();
			ERROR_STREAM << e << std::endl;
			RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
				ssErrorMsg.str().c_str(),
				"MeasurementManager::DMM_MeasurementGettingValues"); 
		}
		catch (...)
		{
			yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
			m_isInternalPb = true;
			std::stringstream ssErrorMsg;
			ssErrorMsg  << "Failed to read the value #" << uiBufferIdx << " (\"" << (it->first) << "\") from the DMM (undetermined error)" << std::endl;
			m_strStatus = ssErrorMsg.str();
			ERROR_STREAM << ssErrorMsg.str();
			THROW_DEVFAILED("DEVICE_ERROR", 
				ssErrorMsg.str().c_str(),
				"MeasurementManager::DMM_MeasurementGettingValues"); 
		}

		dTmpResistance = atof(strTmpRead.c_str()); //- Convert the value from string

		//- Convert resistance to temperature and store it in the local map
		//- Do all the computation (R2T, history, fit)
		(it->second).currentTemp = ResToTemp(dTmpResistance, (it->second).ratedRes, (it->second).ratedTemp, (it->second).ratedBeta);
		(it->second).currentTempHistory.push_back((it->second).currentTemp);
		//- simulate a circular buffer (history buffer):
		if( (it->second).currentTempHistory.size() > m_history_size)
			(it->second).currentTempHistory.pop_front();

		uiBufferIdx++;
	}
}

//--------------------------------------------------------
/**
*	Description: Init the parameters for the calculation of the temperature
*
*	@param oConfigMap Configuration map containing temperature calculation parameter
*/
//--------------------------------------------------------
void MeasurementManager::init_temp_params(Keithley3706ConfigMap_t oConfigMap)
{
	//- Assure that the local map is empty
	m_mapTempParam.clear();

	// Parsing the channels defined in the config map, to define the local map of parameters
	for (Keithley3706ConfigMap_it_t it = oConfigMap.begin(); it != oConfigMap.end(); ++it)
	{
		//- Copy of the parameters (from the sub-structure)
		TempParam oTmpTempParam = (it->second).oTempInfo;

		//- Adding the parameters
		m_mapTempParam[it->first] = oTmpTempParam;
	}

	//- Parsing the config map, to create the list of channels in DMM format
	m_strDMM_Channels = "";
	std::stringstream ssDMM_Channels;

	for (TempParamMap_it_t it = m_mapTempParam.begin(); it != m_mapTempParam.end(); ++it)
	{
		ssDMM_Channels << (it->second).channelId << ",";
	}
	std::string strTmp(ssDMM_Channels.str());
	m_strDMM_Channels = strTmp.substr(0,strTmp.size()-1); //- Erasing the last ","
}
//--------------------------------------------------------
/**
*	Command get_temperature related method
*	Description: Get the temperature of the indicated channel (the last value locally stored).
*
*	@param strAttributeName Identifier of the channel (unique attribute name)
*/
//--------------------------------------------------------
double MeasurementManager::get_temperature(std::string strAttributeName)
{
	double dTmpTemp = yat::IEEE_NAN;
	CHECK_PROXY();
	try
	{
		yat::AutoMutex<> guard(MeasurementManager::m_lock);
		dTmpTemp = m_mapTempParam[strAttributeName].currentTemp;
	}
	catch (Tango::DevFailed &e)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInternalPb = true;
		m_strStatus = "Failed to get temperature";
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, "DEVICE_ERROR",
			m_strStatus.c_str(),
			"MeasurementManager::get_temperature");
	}
	catch (...)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInternalPb = true;
		m_strStatus = "Failed to get temperature (undetermined error)";
		ERROR_STREAM << m_strStatus << std::endl;
		THROW_DEVFAILED("DEVICE_ERROR",
			m_strStatus.c_str(),
			"MeasurementManager::get_temperature");
	}

	return dTmpTemp;
}

//--------------------------------------------------------
/**
*	Command get_temperature_history related method
*	Description: Get the temperature history of the indicated channel.
*
*	@param strAttributeName Identifier of the channel (unique attribute name)
*/
//--------------------------------------------------------
std::deque<double> MeasurementManager::get_temperature_history(std::string strAttributeName)
{
	std::deque<double> dTmpTempHistory;
	CHECK_PROXY();
	try
	{
		yat::AutoMutex<> guard(MeasurementManager::m_lock);
		dTmpTempHistory = m_mapTempParam[strAttributeName].currentTempHistory;
	}
	catch (Tango::DevFailed &e)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInternalPb = true;
		m_strStatus = "Failed to get temperature";
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, "DEVICE_ERROR",
			m_strStatus.c_str(),
			"MeasurementManager::get_temperature");
	}
	catch (...)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInternalPb = true;
		m_strStatus = "Failed to get temperature (undetermined error)";
		ERROR_STREAM << m_strStatus << std::endl;
		THROW_DEVFAILED("DEVICE_ERROR",
			m_strStatus.c_str(),
			"MeasurementManager::get_temperature");
	}

	return dTmpTempHistory;
}

// ============================================================================
// MeasurementManager::periodic_job_i ()
// ============================================================================ 
void MeasurementManager::periodic_job_i()
	throw (Tango::DevFailed)
{
	get_state();

	// don't get new value if Device in FAULT
	if (m_oState != Tango::FAULT)
	{
		// get resistance/temperature from hardware
		try
		{
			DMM_CreateReadingBuffer();
			DMM_Measurement();
		}
		catch (Tango::DevFailed &e)
		{
			yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
			m_isInternalPb = true;
			m_strStatus = "Failed to launch measurement on HW";
			ERROR_STREAM << e << std::endl;
		}
		catch (...)
		{
			yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
			m_isInternalPb = true;
			m_strStatus = "Failed to launch measurement on HW (undetermined error)";
			ERROR_STREAM << m_strStatus << std::endl;
		}
	}
}

//--------------------------------------------------------
/**
*	Description: Converts resistance to temperature (NTC thermistor), with the formula:
*               ( 1.0 /( 1.0 / (dRatedTemp + DELTA_KEVIN_CELSIUS) + log( dRes / dRatedRes ) / dRatedBeta ) - DELTA_KEVIN_CELSIUS)
*
*	@param dRes double& Resistance value to convert
*/
//--------------------------------------------------------
double MeasurementManager::ResToTemp(double dRes, double dRatedRes, double dRatedTemp, double dRatedBeta)
{
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInAlarm = false;
		m_strStatus = "The Keithley is monitoring temperatures";
	}

	//- The calculation will be split for checking division by zero.
	//- Some "range" tests have already be done in the parameters initialization (cf. Keithley3706::get_device_property)
	double dReturnValue;
	double dTmpA;
	double dTmpB;
	double dTmpC;

	dTmpA = 1.0 / (dRatedTemp + DELTA_KEVIN_CELSIUS);
	dTmpB = dRes / dRatedRes;

	if (dTmpB <= 0.0)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		dReturnValue = yat::IEEE_NAN;
		std::stringstream ssErrorMsg;
		ssErrorMsg << "MeasurementManager::ResToTemp / Impossible to convert resistance value to temperature value. (dRes / dRatedRes) must be > 0.0" << std::endl;
		m_isInAlarm = true;
		m_strStatus = ssErrorMsg.str();
	}
	else
	{
		dTmpC = log(dTmpB);
		dTmpC /= dRatedBeta;
		dTmpC += dTmpA;

		if (dTmpC == 0.0)
		{
			yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
			dReturnValue = yat::IEEE_NAN;
			std::stringstream ssErrorMsg;
			ssErrorMsg << "MeasurementManager::ResToTemp / Impossible to convert resistance value to temperature value. (Division by zero)" << std::endl;
			m_isInAlarm = true;
			m_strStatus = ssErrorMsg.str();
		}
		else
		{
			dReturnValue = (1.0 / dTmpC) - DELTA_KEVIN_CELSIUS;
		}
	}

	return dReturnValue;
}

//--------------------------------------------------------
/**
*	Message related to threads management
*	Description: Get the temperature (resistance) of the indicated channel
*
*	@param msg yat::Message& Message to send
*/
//--------------------------------------------------------
void MeasurementManager::process_message (yat::Message& msg)
	throw (Tango::DevFailed)
{
	//- handle msg
	switch (msg.type())
	{
		//- TASK_INIT ----------------------
	case yat::TASK_INIT:
		{
			INFO_STREAM << "MeasurementManager::process_message::TASK_INIT::task is starting up" << std::endl;

			//- Force a 1st read of the temperatures (before the 1st iteration of the periodic msg)
			periodic_job_i(); 
		} 
		break;

		//- TASK_EXIT ----------------------
	case yat::TASK_EXIT:
		{
			INFO_STREAM << "MeasurementManager::process_message::TASK_EXIT::task is quitting" << std::endl;
			// Abort current measure if any
			this->abort();
			
			// delete thread if any
			if (m_poMeasurementScanThread)
			{
				yat::Thread::IOArg ioa;
				m_poMeasurementScanThread->exit();
				m_poMeasurementScanThread->join(&ioa);
				INFO_STREAM << "MeasurementManager::process_message - Measurement Scan thread exited" << std::endl;
				m_poMeasurementScanThread = NULL; 
			}
		}
		break;

		//- TASK_PERIODIC ------------------
	case yat::TASK_PERIODIC:
		{
			DEBUG_STREAM << "MeasurementManager::process_message::TASK_PERIODIC" << std::endl;
			periodic_job_i();
		}
		break;

		//- TASK_TIMEOUT -------------------
	case yat::TASK_TIMEOUT:
		{
			//- not used
		}
		break;
		//- End of the measurements on HW
	case kEND_OF_HW_MEASUREMENTS:
		{
			DEBUG_STREAM << "MeasurementManager::process_message::kEND_OF_HW_MEASUREMENTS" << std::endl;
			//- After scanning the resistance values (in the dedicated thread), they are read from the HW buffer
			if (!get_ScanAborted())
			{
			  DMM_MeasurementGettingValues();
			}

			// Delete thread
			DEBUG_STREAM << "MeasurementManager::process_message - Asking Measurement Scan thread to quit" << std::endl;
			if (m_poMeasurementScanThread)
			{
				yat::Thread::IOArg ioa;
				m_poMeasurementScanThread->exit();
				m_poMeasurementScanThread->join(&ioa);
				DEBUG_STREAM << "MeasurementManager::process_message - Measurement Scan thread exited" << std::endl;
				m_poMeasurementScanThread = NULL; 
			}
		}
		break;
		//- UNHANDLED MSG --------------------
	default:
		DEBUG_STREAM << "MeasurementManager::process_message::unhandled msg type received" << std::endl;
		break;
	}
}

//--------------------------------------------------------
/**
*	Reset
*	Description: Command to reinit the device
*/
//--------------------------------------------------------
void MeasurementManager::reset ()
{
	INFO_STREAM << "MeasurementManager::reset: entering... !" << std::endl;

	CHECK_PROXY();

	try
	{
		DMM_CommandWrite("reset()");
	}
	catch (Tango::DevFailed &e)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInternalPb = true;
		m_strStatus = "Failed to reset on HW";
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			m_strStatus.c_str(), 
			"MeasurementManager::reset"); 
	}
	catch (...)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInternalPb = true;
		m_strStatus = "Failed to reset on HW (undetermined error)";
		ERROR_STREAM << m_strStatus << std::endl;
		THROW_DEVFAILED("DEVICE_ERROR", 
			m_strStatus.c_str(), 
			"MeasurementManager::reset"); 
	}
}

//--------------------------------------------------------
/**
*	Abort
*	Description: Stops the background process of reading the values
*/
//--------------------------------------------------------
void MeasurementManager::abort ()
{
	INFO_STREAM << "MeasurementManager::abort: entering... !" << std::endl;

	CHECK_PROXY();

	try
	{
		DMM_CommandWrite("scan.abort()");

		//- Stopping the task
		enable_periodic_msg(false);

		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInAlarm = true;
		m_bScanAborted = true;
		m_strStatus = "Scan aborted.";

		DEBUG_STREAM << "MeasurementManager::abort / State = " << m_oState << " / Status = " << m_strStatus << std::endl;
	}
	catch (Tango::DevFailed &e)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInternalPb = true;
		m_strStatus = "Abort failed";
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			m_strStatus.c_str(), 
			"MeasurementManager::abort"); 
	}
	catch (...)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInternalPb = true;
		m_strStatus = "Abort failed (undetermined error)";
		ERROR_STREAM << m_strStatus << std::endl;
		THROW_DEVFAILED("DEVICE_ERROR", 
			m_strStatus.c_str(), 
			"MeasurementManager::abort"); 
	}
}

//--------------------------------------------------------
/**
*	Start
*	Description: Restarts the background process of reading the values
*/
//--------------------------------------------------------
void MeasurementManager::start ()
{
	INFO_STREAM << "MeasurementManager::start: entering... !" << std::endl;

	// Check if acquisition is in progress. If so, nothing to do...
	if (!this->periodic_msg_enabled())
	{
		// re-start periodic task
		this->enable_periodic_msg(true);

		// reset abort flag
		{
			yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
			m_isInAlarm = false;
			m_bScanAborted = false;
			m_strStatus = "The Keithley is monitoring temperatures";
			m_oState = Tango::RUNNING;			
		}
	}
}

//--------------------------------------------------------
/**
*	Description: Send one command "Write" to the hardware
*
*	@param pcCmdLine (const char*) Command line
*/
//--------------------------------------------------------
void MeasurementManager::DMM_CommandWrite(const char* pcCmdLine)
{
	std::string strCmdLine = std::string(pcCmdLine);
	std::stringstream ssCmdLine;

	ssCmdLine << strCmdLine;
	DMM_CommandWrite(ssCmdLine);
}

//--------------------------------------------------------
/**
*	Description: Send one command "Write" to the hardware
*
*	@param ssCmdLine (std::stringstream) Command line
*/
//--------------------------------------------------------
void MeasurementManager::DMM_CommandWrite(const std::stringstream& ssCmdLine)
{
	std::string strCmdLine = ssCmdLine.str();
	try
	{
		DEBUG_STREAM << "++  DMM_CommandWrite / ssCmdLine = \"" << strCmdLine << "\"" << std::endl;
		Tango::DeviceData oDataToWrite;
		oDataToWrite << strCmdLine;
		m_hw_proxy->command_inout("Write",oDataToWrite);
	}
	catch (Tango::DevFailed &e)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInternalPb = true;
		std::stringstream ssErrorMsg;
		ssErrorMsg << "Command for DMM not accepted (\"" << strCmdLine << "\")";
		m_strStatus = ssErrorMsg.str();
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			ssErrorMsg.str().c_str(),
			"MeasurementManager::DMM_CommandWrite"); 
	}
	catch (...)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInternalPb = true;
		std::stringstream ssErrorMsg;
		ssErrorMsg << "Command for DMM not accepted (\"" << strCmdLine << "\") (undetermined error)";
		m_strStatus = ssErrorMsg.str();
		ERROR_STREAM << ssErrorMsg.str();
		THROW_DEVFAILED("DEVICE_ERROR", 
			ssErrorMsg.str().c_str(), 
			"MeasurementManager::DMM_CommandWrite"); 
	}
}

//--------------------------------------------------------
/**
*	Description: Send one command "WriteRead" to the hardware
*
*	@param pcCmdLine (const char*) Command line
*	@return (Tango::DeviceData) Information read
*/
//--------------------------------------------------------
Tango::DeviceData MeasurementManager::DMM_CommandWriteRead(const char* pcCmdLine)
{
	std::string strCmdLine = std::string(pcCmdLine);
	std::stringstream ssCmdLine;

	ssCmdLine << strCmdLine;

	return(DMM_CommandWriteRead(ssCmdLine));
}

//--------------------------------------------------------
/**
*	Description: Send one command "WriteRead" to the hardware
*
*	@param ssCmdLine (std::stringstream) Command line
*	@return (Tango::DeviceData) Information read
*/
//--------------------------------------------------------
Tango::DeviceData MeasurementManager::DMM_CommandWriteRead(const std::stringstream& ssCmdLine)
{
	std::string strCmdLine = ssCmdLine.str();
	Tango::DeviceData oDataToRead;

	try
	{
		DEBUG_STREAM << "MeasurementManager::DMM_CommandWriteRead / ssCmdLine = \"" << strCmdLine << "\"" << std::endl;
		Tango::DeviceData oDataToWrite;		

		oDataToWrite << strCmdLine;
		oDataToRead = m_hw_proxy->command_inout("WriteRead",oDataToWrite);
	}
	catch (Tango::DevFailed &e)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInternalPb = true;
		std::stringstream ssErrorMsg;
		ssErrorMsg << "Command for DMM not accepted (\"" << strCmdLine << "\")";
		m_strStatus = ssErrorMsg.str();
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			ssErrorMsg.str().c_str(),
			"MeasurementManager::DMM_CommandWriteRead"); 
	}
	catch (...)
	{
		yat::AutoMutex<> guard(MeasurementManager::m_oLockStateStatus);
		m_isInternalPb = true;
		std::stringstream ssErrorMsg;
		ssErrorMsg << "Command for DMM not accepted (\"" << strCmdLine << "\") (undetermined error)";
		m_strStatus = ssErrorMsg.str();
		ERROR_STREAM << ssErrorMsg.str();
		THROW_DEVFAILED("DEVICE_ERROR", 
			ssErrorMsg.str().c_str(), 
			"MeasurementManager::DMM_CommandWriteRead"); 
	}

	return oDataToRead;
}

} // namespace Keithley3706_ns
