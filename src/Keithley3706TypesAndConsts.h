//=============================================================================
// Keithley3706TypesAndConsts.h
//=============================================================================
// abstraction.......Keithley3706 
// class.............Keithley3706TypesAndConsts
// original author.... E.LOMBARD - NEXEYA
//=============================================================================

#ifndef _KEITHLEY3706_TYPES_AND_CONSTS_H_
#define _KEITHLEY3706_TYPES_AND_CONSTS_H_

#pragma once

//=============================================================================
// DEPENDENCIES
//=============================================================================
#include <tango.h>
#include <sstream>
#include <map>
#include <deque>

#include <yat/utils/XString.h>

//=============================================================================
// IMPL OPTION
//=============================================================================

namespace Keithley3706_ns
{

const int SINGLE_DATA = 1;
//- Allowed string delimiters for each property to be parsed
static std::string DELIMS = "\t;|:/";
static size_t MIN_EXPECTED_TOKEN_TEMP_ATTR_LIST = 5; //- 5 = Attribute name + ChannelId + Format + Description + Unit
static size_t MIN_EXPECTED_TOKEN_TEMP_CFG = 4; //- 4 = Attribute name + Rated res + Rated temp + Rated beta

//- Value for temperature calculation
#define DELTA_KEVIN_CELSIUS 273.0

//- name for range configuration
#define RANGE_CONFIG "NTC_10kOhm"

// ============================================================================
// TempParam:  struct containing the temperature parameters
// It contains NTCConfig data (cf documentation)
// It stores the current calculated temperature (for MeasurementManager)
// ============================================================================
struct TempParam
{
	//- Attribute name (unique, and stored in the map type Keithley3706ConfigMap_t (see Keithley3706ConfigMap_t)

	//- Channel Identifier used by the Keithley
	unsigned int channelId;

	//- Rated resistance (same unit as read resistance)
	double ratedRes;

	//- Rated temperature (degree C)
	double ratedTemp;

	//- Rated beta (degree K)
	double ratedBeta;

	//------------------------------------------------------
	// Current temperature
	double currentTemp;

	//- currentTemp History (used for drift calculation)
	std::deque<double> currentTempHistory;
	
	TempParam():
	channelId(0),
	ratedRes(1.0),
	ratedTemp(1.0),
	ratedBeta(1.0),
	currentTemp(0.0)
	{
	}

	TempParam (const TempParam & src)
	{
		*this = src;
	}

	const TempParam & operator= (const TempParam & src)
	{
		channelId = src.channelId;
		ratedRes = src.ratedRes;
		ratedTemp = src.ratedTemp;
		ratedBeta = src.ratedBeta;
		currentTemp = src.currentTemp;
		return *this;
	}

	void dump()
	{
		cout << "channelId   : " << channelId << endl;
		cout << "ratedRes    = " << ratedRes << endl;
		cout << "ratedTemp   = " << ratedTemp << endl;
		cout << "ratedBeta   = " << ratedBeta << endl;
		cout << "currentTemp = " << currentTemp << endl;
	}
};

//- map type for storing only parameters for temperature calculation - key = attributeName (string type)
typedef std::map<std::string, TempParam> TempParamMap_t;
typedef TempParamMap_t::iterator TempParamMap_it_t;

// ============================================================================
// Keithley3706Config:  struct containing the temperature sensor definition data
// It merges the data of TemperatureAttributeList and NTCConfig
// It stores the current read temperature (asked by the read_Temperature function)
// ============================================================================
struct Keithley3706Config
{
	// Values used to define DynamicAttributeInfo
	//- Attribute name (unique, and stored in the map type Keithley3706ConfigMap_t (see Keithley3706ConfigMap_t)

	//- Attribute format (ex : %2d, %6.2f)
	std::string format;

	//- Attribute description
	std::string description;

	//- Attribute unit
	std::string unit;

	//------------------------------------------------------
	// Additionnal temperature/attribute/channel parameters
	TempParam oTempInfo;

	Keithley3706Config():
	format(""),
	description(""),
	unit("")
	{
	}

	Keithley3706Config (const Keithley3706Config & src)
	{
		*this = src;
	}

	const Keithley3706Config & operator= (const Keithley3706Config & src)
	{
		format = src.format;
		description = src.description;
		unit = src.unit;
		oTempInfo = src.oTempInfo;
		return *this;
	}

	void dump()
	{
		cout << "format      : " << format << endl;
		cout << "description : " << description << endl;
		cout << "unit        : " << unit << endl;
		oTempInfo.dump();
	}
};

//- map type for storing temperature sensor definition - key = attributeName (string type)
// The key is used to define DynamicAttributeInfo "name"
typedef std::map<std::string, Keithley3706Config> Keithley3706ConfigMap_t;
typedef Keithley3706ConfigMap_t::iterator Keithley3706ConfigMap_it_t;

} // namespace Keithley3706_ns

#endif // _KEITHLEY3706_TYPES_AND_CONSTS_H_