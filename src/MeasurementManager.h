#pragma once
//=============================================================================
// MeasurementManager.h
//=============================================================================
// abstraction.......MeasurementManager for Keithley
// class.............MeasurementManager
// original author...E.Lombard - Nexeya
//=============================================================================

#ifndef _MEASUREMENT_MANAGER_H
#define _MEASUREMENT_MANAGER_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat4tango/DeviceTask.h>
#include "Keithley3706TypesAndConsts.h"

// ============================================================================
// SOME USER DEFINED MESSAGES FOR THE CountingManager task
// ============================================================================
#define kEND_OF_HW_MEASUREMENTS (yat::FIRST_USER_MSG + 1001)

namespace Keithley3706_ns
{
	// ============================================================================
	// struct: MeasureScanConfig
	// Configuration of the end of measurement loop task
	// ============================================================================
	struct MeasureScanConfig
	{
		//- the TANGO host device (for logging)
		Tango::DeviceImpl * hostDevice;

		//- default ctor (set default values)
		MeasureScanConfig () :
		hostDevice(NULL)
		{
			//-noop ctor
		}

		//- copy ctor 
		MeasureScanConfig (const MeasureScanConfig & src)
		{
			*this = src;
		}

		//- operator=
		const MeasureScanConfig & operator= (const MeasureScanConfig & src)
		{
			if (&src == this)
				return *this;
			this->hostDevice = src.hostDevice;
			return *this;
		}

	};

	// ============================================================================
	// class: MeasureScanThread
	// ============================================================================
	class MeasureScanThread : public yat::Thread, public Tango::LogAdapter
	{
		friend class MeasurementManager;

	protected:
		//- ctor ---------------------------------
		MeasureScanThread (const MeasureScanConfig & oCfg, yat::Thread::IOArg ioa);

		//- dtor ---------------------------------
		virtual ~MeasureScanThread (void);

		//- thread's entry point
		virtual yat::Thread::IOArg run_undetached (yat::Thread::IOArg ioa);

		//- asks this MeasureScanThread to quit
		virtual void exit (void);

	private:
		//- the task config
		MeasureScanConfig m_cfg;

		//- thread's ctrl flag
		bool m_goOn;
	};




	// ============================================================================
	// class: MeasurementManager
	// ============================================================================
	class MeasurementManager : public yat4tango::DeviceTask
	{
	public:
		//- Constructor
		MeasurementManager(Tango::DeviceImpl * hostDevice);

		//- Destructor
		~MeasurementManager(void);

		//- gets  state
		Tango::DevState get_state();

		//- gets status
		std::string get_status();

		//- Init
		void init_hw(std::string strProxyName, std::string strSettingFileName, Keithley3706ConfigMap_t oConfigMap, double dPollingPeriod, double historyDurationInHour);

		//- Reset DMM
		void reset();

		//- Abort scan
		void abort();

		//- Restart scan
		void start();
		
		//- Get the temperature
		double get_temperature(std::string strAttributeName);

		//- Get the temperature history
		std::deque<double> get_temperature_history(std::string strAttributeName);

		//- DMM waiting loop for complete measurement (public to be usable by the thread)
		void DMM_MeasurementWaitingLoop(void);


	protected:
		//- device proxy
		Tango::DeviceProxy * m_hw_proxy;

		//- process_message (implements yat4tango::DeviceTask pure virtual method)
		virtual void process_message (yat::Message& msg)
			throw (Tango::DevFailed);

		//- Init the parameters for the calculation of the temperature
		void init_temp_params(Keithley3706ConfigMap_t oConfigMap);

		//- DMM setup
		void DMM_Setup(std::string strSettingFileName);

		//- DMM reading buffer creation
		void DMM_CreateReadingBuffer(void);

		//- DMM measurement
		void DMM_Measurement(void);

		//- DMM measurement initialization
		void DMM_MeasurementInitialization(void);

		//- DMM getting measurement values
		void DMM_MeasurementGettingValues(void);

		//- DMM write command
		void DMM_CommandWrite(const char* pcCmdLine);

		//- DMM write command
		void DMM_CommandWrite(const std::stringstream& ssCmdLine);

		//- DMM WriteRead command
		Tango::DeviceData DMM_CommandWriteRead(const char* pcCmdLine);

		//- DMM WriteRead command
		Tango::DeviceData DMM_CommandWriteRead(const std::stringstream& ssCmdLine);

		//- Get the flag about scan abort
		bool get_ScanAborted();
		
		//- Update state
		void update_state();

		//- gets device state
		Tango::DevState get_device_state();

		//- general state
		Tango::DevState m_oState;

		//- status
		std::string m_strStatus;

		//- Map containing the temperature parameters
		TempParamMap_t m_mapTempParam;

		//- String containing the list of channels in DMM format
		std::string m_strDMM_Channels;

		//- Host device
		Tango::DeviceImpl * m_oHostDevice;

		//- End of HW measurement waiting task
		MeasureScanThread * m_poMeasurementScanThread;

		//----------------------------------------------------
		//- periodic job
		void periodic_job_i ()
			throw (Tango::DevFailed);

		//- Conversion from resistance to temperature
		double ResToTemp(double dRes, double dRatedRes, double dRatedTemp, double dRatedBeta);

	private:
		//- to avoid race condition between XBPM class & periodic_job
		yat::Mutex m_lock;

		//- to protect r/w of state or status
		yat::Mutex m_oLockStateStatus;

		// alarm flag
		bool m_isInAlarm;

		//- internal pb flag
		bool m_isInternalPb;

		//- Abort state flag
		bool m_bScanAborted;

		//- history size
		double m_history_size;

	};

} // namespace Keithley3706_ns

#endif // _MEASUREMENT_MANAGER_H
